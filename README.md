### Spring Boot Projekt - Literaturverwaltung

Einfaches Projekt mit CRUD-Funktionalität, wobei User webbasiert Literaturlisten pflegen können. User-Eingaben erfolgen mit einer Eingabemaske im Browser. Diese werden in einer SQL-Datenbank (PostgreSQL) persistiert.

## Tech-Stack:

Frontend: HTML5 mit Template Engine Thymeleaf

Backend: Java 17 mit Spring Boot

Datenbank: PostgreSQL 9.5 
aktuell auf lokalen Rechner (Ubuntu), später noch containerisiert

## To Do's:

[x] Datenbank aufgesetzt  
[x] Backend aufgesetzt und installiert  
[x] HTML-Templates angelegt  
[x] Build-Pipeline bei Gitlab eingerichtet  
[ ] Tests (Unit-Tests, Integrationstests) schreiben  
[ ] Update-Funktionalität einfügen  
[ ] DB in Container aufsetzen  