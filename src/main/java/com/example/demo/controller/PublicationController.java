package com.example.demo.controller;

import com.example.demo.entity.Publication;
import com.example.demo.service.PublicationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping

public class PublicationController {

    private final PublicationService service;
    @Autowired
    public PublicationController(PublicationService service){
        this.service = service;
    }

    @GetMapping("/test")
    public ModelAndView test(){
        ModelAndView m = new ModelAndView("base.html");
        return m;
    }

    @GetMapping("/api/v1/publications/list")
    public ModelAndView getAllPublications(){
        ModelAndView mav = new ModelAndView("list-publications.html");
        mav.addObject("publications", service.getAllPublications());
        return mav;
    }

    @GetMapping("/api/v1/showupdateform")
    public ModelAndView updatePublications(){
        ModelAndView mv = new ModelAndView("add-publication.html");
        Publication pub = new Publication();
        mv.addObject("publication", pub);
        return mv;

    }
    /*
    IN PROGRESS
     */


    @GetMapping("/api/v1/publications/")
    public ModelAndView getSpecificPublication(@RequestParam String title){
        ModelAndView mav = new ModelAndView("list-publications.html");
        mav.addObject("persons", service.getSpecificPublication(title));
        return mav;

    }

    @PostMapping("/insertpublication")
    public String insertPublication(@ModelAttribute Publication publication){
        this.service.savePublication(publication);
        return "redirect:/api/v1/publications/list";
    }
    @GetMapping("/api/v1/deletepublication")
    public String deletePublication(@RequestParam String title){
        this.service.deletePublication(title);
        return "redirect:/api/v1/publications/list";
    }


}
