package com.example.demo.entity;

import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import org.springframework.lang.NonNull;


import java.time.LocalDate;
import java.util.List;
@Entity
//@Table(name="publications")
public class Publication {
    @Id
    @NonNull
    private String title;
    private LocalDate publishingDate;
    //private String publishingDate;
    @ElementCollection
    private List<String> authors;

    private boolean alreadyRead;

    public boolean isAlreadyRead() {
        return alreadyRead;
    }

    public void setAlreadyRead(boolean alreadyRead) {
        this.alreadyRead = alreadyRead;
    }




    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public LocalDate getPublishing_date() {
        return publishingDate;
    }

    public String getDateAsString(){
        return this.publishingDate.toString();
    }

    public void setPublishing_date(LocalDate publishing_date) {
        this.publishingDate = publishing_date;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    @Override
    public String toString() {
        return "Publication{" +
                "title='" + title + '\'' +
                ", publishingDate='" + publishingDate + '\'' +
                ", authors=" + authors + '\'' +
                ", alread read?=" + alreadyRead +
                '}';
    }
}
