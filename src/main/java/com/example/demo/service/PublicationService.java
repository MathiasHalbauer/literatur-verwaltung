package com.example.demo.service;

import com.example.demo.dao.PublicationRepository;
import com.example.demo.entity.Publication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PublicationService {

    private final PublicationRepository repo;
    @Autowired
    public PublicationService(PublicationRepository repo){
        this.repo = repo;
    }

    public List getAllPublications(){
        return this.repo.findAll().stream().toList();
    }

    public Optional<Publication> getSpecificPublication(String title){
        return this.repo.findById(title);

    }
    /*
    To Do: Implementiere mit JDBC-Template

    public List<Publication> getAllPublicationsByAuthor(String author){
        String myQuery = "SELECT * FROM PUBLICATIONS WHERE AUTHOR = ?";
        JdbcTemplate templ = new JdbcTemplate();
        templ.query(myQuery)

    }
    */


    public void savePublication(Publication myPublication){
        //Date myPublication.publishingDate;
        Optional<Publication> publicationRecord = getSpecificPublication(myPublication.getTitle());

        if (publicationRecord.isEmpty()){

            this.repo.save(myPublication);
        }

        else{
            return;
        }
    }

    public void deletePublication(@NonNull String title){
        this.repo.deleteById(title);
    }

    //deleteResource

}
